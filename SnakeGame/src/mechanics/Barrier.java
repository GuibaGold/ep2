/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mechanics;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 *
 * @author guibd
 */
public class Barrier {
    private int xCoor, yCoor, width, height;
    public Barrier(int xCoor, int yCoor, int titleSize) {
        this.xCoor = xCoor;
        this.yCoor = yCoor;
        this.width = titleSize;
        this.height = titleSize;
    }
    
    public static void createBarrier(ArrayList<Barrier> barrier){
        for(int i = 20; i < 30; i++){
            barrier.add(new Barrier(i, 25, 10));
            barrier.add(new Barrier(i, 26, 10));
        }
    }
    
    public int getxCoor() {
        return xCoor;
    }

    public void setxCoor(int xCoor) {
        this.xCoor = xCoor;
    }

    public int getyCoor() {
        return yCoor;
    }

    public void setyCoor(int yCoor) {
        this.yCoor = yCoor;
    }
    public void draw(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(xCoor*width, yCoor*height, width, height);
    }
    
    
    
}
