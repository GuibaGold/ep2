/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mechanics;

import javax.swing.JOptionPane;

/**
 *
 * @author guibd
 */
public class StarSnake extends Snake {

    @Override
    public void tick() {
        if(this.isShowing()){
            super.snakeMechanic();
            fruitMechanicStar();
            bigFruitMechanic();
            decreaseMechanic();
            bombFruitMechanic();

            if(super.selfCollision()){
                JOptionPane.showMessageDialog(this, "GAME OVER \n\n Pontuação: " + points);
                if(JOptionPane.showConfirmDialog(this, "Game Over!\nDeseja continuar?") == JOptionPane.OK_OPTION){
                    this.disable(); 
                    new Snake();
                }else{
                   stop(); 
                   this.setVisible(false);
                   this.disable();
                }
            }

            if(super.wallCollision()) {
                JOptionPane.showMessageDialog(this, "GAME OVER \n\n Pontuação: " + points);
                if(JOptionPane.showConfirmDialog(this, "Game Over\nDeseja continuar?") == JOptionPane.OK_OPTION){
                    this.disable(); 
                    new Snake();
                }else{
                   stop(); 
                   this.setVisible(false);
                   this.disable();
                }
            }

            if(barrierCollision()){
                JOptionPane.showMessageDialog(this, "GAME OVER \n\n Pontuação: " + points);
                 if(JOptionPane.showConfirmDialog(this, "Game Over\nDeseja continuar?") == JOptionPane.OK_OPTION){
                    this.disable(); 
                    new Snake();
                }else{
                   stop(); 
                   this.setVisible(false);
                   this.disable();
                }
            }
            
        }
    }

    
    private void fruitMechanicStar() {
        if(simpleFruits.size() == 0) {
            int xCoor = r.nextInt(49);
            int yCoor = r.nextInt(49);

            Fruit fruit = new Fruit(xCoor, yCoor, 10);
            simpleFruits.add(fruit);
        }

        for(int i = 0; i < simpleFruits.size(); i++) {
            if(xCoor == simpleFruits.get(i).getxCoor() && yCoor == simpleFruits.get(i).getyCoor()) {
                size += 4;
                simpleFruits.remove(i);
                i++;
                points++;
                super.points += size * 100;
            }
        } 
    }
}
