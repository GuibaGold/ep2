package mechanics;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class BigFruit extends Fruit {

	public BigFruit(int xCoor, int yCoor, int titleSize) {
		super(xCoor, yCoor, titleSize);
	}
	
	
	
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRoundRect(xCoor*width, yCoor*height, width, height, width, height);
	}
	
}
