package mechanics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Snake extends JPanel implements Runnable, KeyListener{

	private static final long serialVersionUID = 1L;
	
	protected static final int WIDTH = 500, HEIGHT = 500;
	
	private Thread thread;
	
	private boolean running;
	
	private boolean right = true, left = false, up = false, down = false;
	
	private BodyPart b;
	protected ArrayList<BodyPart> snake;
	
	Fruit fruit;
	protected ArrayList<Fruit> simpleFruits;
	
	BombFruit bombFruit;
	protected ArrayList<BombFruit> bombFruits;
	
	BigFruit bigFruit;
	private ArrayList<BigFruit> bigFruits;
	
	DecreaseFruit decreaseFruit;
	private ArrayList<DecreaseFruit> decreaseFruits;
	
	
	protected Random r;
        
    private ArrayList<Barrier> barrier;
	
	protected int yCoor = 10, xCoor = 10, size  = 10, points = 0;

	private int ticks = 0;
	
	public Snake() {
            setFocusable(true);

            setPreferredSize(new Dimension(WIDTH, HEIGHT));
            addKeyListener(this);

            snake = new ArrayList<BodyPart>();
            simpleFruits = new ArrayList<Fruit>();
            bombFruits = new ArrayList<BombFruit>();
            bigFruits = new ArrayList<BigFruit>();
            decreaseFruits = new ArrayList<DecreaseFruit>();
            barrier = new ArrayList<Barrier>();
            Barrier.createBarrier(barrier);

		
            r = new Random();

            start();

	}
        
        
	
	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        }
        
    public void snakeMechanic(){
        if(snake.size() == 0) {
            b = new BodyPart(xCoor, yCoor, 10);
            snake.add(b);
        }
        ticks++;
        if(ticks > 250000) {
                if(right) xCoor++;
                if(left) xCoor--;
                if(up) yCoor--;
                if(down) yCoor++;

                ticks = 0;

                b = new BodyPart(xCoor, yCoor, 10);
                snake.add(b);


                if(snake.size() > size) {
                        snake.remove(0);
                }
        }
    }
    
    public void decreaseMechanic() {
        if(decreaseFruits.size() == 0) {
            int xCoor = r.nextInt(49);
            int yCoor = r.nextInt(49);

            decreaseFruit = new DecreaseFruit(xCoor, yCoor, 10);
            decreaseFruits.add(decreaseFruit);
        }
        
        for(int i = 0; i < decreaseFruits.size(); i++) {
            if(xCoor == decreaseFruits.get(i).getxCoor() && yCoor == decreaseFruits.get(i).getyCoor()) {
               
                snake.clear();
                
                decreaseFruits.remove(i);
                i++;
                
                points++;
                size = 10;
                snakeMechanic();

            }
        }

    }
    public void bigFruitMechanic() {
        if(bigFruits.size() == 0) {
           bigFruit = new BigFruit(r.nextInt(49), r.nextInt(49), 10);
           bigFruits.add(bigFruit);
       }

       for(int i = 0; i < bigFruits.size(); i++) {
           if(xCoor == bigFruits.get(i).getxCoor() && yCoor == bigFruits.get(i).getyCoor()) {
               size += 5;
               bigFruits.remove(i);
               i++;
               points++;
               points += size * 200;
           }
       }

    }
    public void fruitMechanic(){
        if(simpleFruits.size() == 0) {
            fruit = new Fruit(r.nextInt(49), r.nextInt(49), 10);
            simpleFruits.add(fruit);
        }

        for(int i = 0; i < simpleFruits.size(); i++) {
                if(xCoor == simpleFruits.get(i).getxCoor() && yCoor == simpleFruits.get(i).getyCoor()) {
                    size += 5;
                    simpleFruits.remove(i);
                    i++;
                    points++;
                    points += size * 100;
                }
        }
        
    }
        
	public void tick() {
            if(this.isShowing()){
                
                snakeMechanic();
                fruitMechanic();
                bigFruitMechanic();
                decreaseMechanic();
                bombFruitMechanic();
                
                if(selfCollision()){
                    gameOver();
                }

                if(wallCollision()) {
                    gameOver();
                }

                if(barrierCollision()){
                    gameOver();
                }
            }
            
	}
        
        public void bombFruitMechanic() {
            if(bombFruits.size() == 0) {
            int xCoorFruit = r.nextInt(49);
            int yCoorFruit = r.nextInt(49);

            bombFruit = new BombFruit(xCoorFruit, yCoorFruit, 10);
            bombFruits.add(bombFruit);
        }

        for(int i = 0; i < bombFruits.size(); i++) {
            if(xCoor == bombFruits.get(i).getxCoor() && yCoor == bombFruits.get(i).getyCoor()) {
                bombFruits.remove(i);
                gameOver();
            }
        }
		
	}
        
        protected void gameOver() {
        JOptionPane.showMessageDialog(this, "GAME OVER \n\n Pontuação: " + points);
        if(JOptionPane.showConfirmDialog(this, "Game Over!\nDeseja continuar?") == JOptionPane.OK_OPTION){
            this.disable(); 
            new Snake();
        }else{
           stop(); 
           this.setVisible(false);
           this.disable();
        }
	}
        protected boolean barrierCollision(){
            for(int i = 0; i < barrier.size(); i++) {
                if(xCoor == barrier.get(i).getxCoor() && yCoor == barrier.get(i).getyCoor()) {
                    if(i != barrier.size() - 1) {
                        return true;
                    } 
                }
            }
            return false;
        }
        
        protected boolean selfCollision(){
            for(int i = 0; i < snake.size(); i++) {
                if(xCoor == snake.get(i).getxCoor() && yCoor == snake.get(i).getyCoor()) {
                        if(i != snake.size() - 1) {
                            return true;
                        } 
                }
            }
            
            return false;
        }
        
        protected boolean wallCollision(){
            if(xCoor < 0 || xCoor > 49 || yCoor < 0 || yCoor > 49) {
                return true;
            } else{
                return false;
            }
        }
	
	public void paint(Graphics g) {
		g.clearRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		for(int i = 0; i < WIDTH/10; i++) {
			g.drawLine(i * 10, 0, i * 10, HEIGHT);
		}
		
		for(int i = 0; i < HEIGHT/10; i++) {
			g.drawLine(0, i * 10, HEIGHT, i * 10);
		}
		
		for(int i = 0; i < snake.size(); i++) {
			snake.get(i).draw(g);
		}
		
		for(int i = 0; i < simpleFruits.size(); i++) {
			simpleFruits.get(i).draw(g);
		}
                
        for(int i = 0; i < barrier.size(); i++){
            barrier.get(i).draw(g);
        }
        for(int i = 0; i < bombFruits.size(); i++){
            bombFruits.get(i).draw(g);
        }
        for(int i = 0; i < bigFruits.size(); i++){
            bigFruits.get(i).draw(g);
        }
        for(int i = 0; i < decreaseFruits.size(); i++){
            decreaseFruits.get(i).draw(g);
        }
        
	}

	@Override
	public void run() {
            
            while(running){
                tick();
                this.repaint();
		
            }
        }

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if(key == KeyEvent.VK_RIGHT && !left) {
			right = true;
			up = false;
			down = false;
		}
		if(key == KeyEvent.VK_LEFT && !right) {
			left = true;
			up = false;
			down = false;
		}
		if(key == KeyEvent.VK_UP && !down) {
			up = true;
			right = false;
			left = false;
		}
		if(key == KeyEvent.VK_DOWN && !up) {
			down = true;
			right = false;
			left = false;
		}
                if(key == KeyEvent.VK_ESCAPE){
                    stop();
                }
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
