package mechanics;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class Fruit {
	
	protected int xCoor;
	protected int yCoor;
	protected int width;
	protected int height;
	
	public Fruit(int xCoor,int yCoor, int titleSize) {
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		this.width = titleSize;
		this.height = titleSize;
	}
        
   
	
	public int getxCoor() {
		return xCoor;
	}

	public void setxCoor(int xCoor) {
		this.xCoor = xCoor;
	}

	public int getyCoor() {
		return yCoor;
	}

	public void setyCoor(int yCoor) {
		this.yCoor = yCoor;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.RED);
		g.fillRoundRect(xCoor*width, yCoor*height, width, height, width, height);
	}
}
