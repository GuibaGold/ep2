package mechanics;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class BombFruit extends Fruit{

	public BombFruit(int xCoor, int yCoor, int titleSize) {
		super(xCoor, yCoor, titleSize);
		
	}
	
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRoundRect(xCoor*width, yCoor*height, width, height, width, height);
	}
	
	

}
